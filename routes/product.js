/*
* Using mongoose ORM for Mongodb here !
*/
var mongoose = require('mongoose'),
db = mongoose.connect('mongodb://localhost/products'),
Schema = mongoose.Schema;

/*
* Creating Schema for Products document !
*/
var productSchema = new Schema({
  category: String,
  companyId: Number,
  price: Number
});

/*
* Mongoose ORM provides modelling your documents to ease life !
*/
var Product = mongoose.model('Product', productSchema);

/*
	validations
*/

Product.schema.path('category').validate(function (value) {
	return value.length > 0;
}, "Category cannot be blank !");

Product.schema.path('price').validate(function (value) {
	return _.isNumber( value );
},"Price should be a number !");

/*
*  Get a list of all the products. The index action
*  Route : /products
*/
exports.list = function(req, res) {
  Product.find({}, function(err, products) {
    if (err) {
      throw err;
    } else{
      res.render('products/index',{ title: "All products" , products: products });
    };
  });
};

/*
* Create a new product
* Route : GET /product/new
*/

exports.new = function(req, res) {
  res.render('products/new', {title: "New Product"});
};

/*
* Create the product
* Route : POST /product
*/
exports.create = function(req, res) {
  var product = new Product;
  console.log(req.body);
  product.category = req.body.category;
  product.price = req.body.price;
  product.companyId = 33;
  product.save(function(err) {
    if (err) {
      throw err;
    } else{
      res.redirect('products');
    };
  });
};

/*
* Edit the product
* Route : GET /product/:id/edit
*/
exports.edit = function(req, res) {
	Product.findById(req.params.id, function  (err, product) {
		if (err) {
			throw err;
		} else{
			return res.render('products/edit',{title: "Edit", product: product})
		};
	});
};

/*
* Update the product
* Route : PUT /product/:id/update
*/

exports.update = function(req, res) {
	
	Product.findById( req.params.id, function(err, product) {
		if (err) {
			throw err;
		} else{
			product.category = req.body.category;
			product.price = req.body.price;
			product.save(function(err) {
				if (err) {
					throw err;
				} else{
					res.redirect('products')
				};
			});
		};
	});
};

/*
* Show a product
* Route : GET /product/:id
*/
exports.show = function(req, res) {
	Product.findById(req.params.id, function  (err, product) {
		if (err) {
			throw err;
		} else{
			return res.render('products/show',{title: "Edit", product: product})
		};
	});
};

/*
* Deleting a product
* Route : DELETE /product/:id/delete
*/

exports.delete = function(req, res) {
	product = Product.findById( req.params.id, function (err, product) {
		if (err) {
			throw err;
		} else{
			product;
		};
	});
	product.remove( function(err) {
		if (err) {
			throw err;
		} else{
			res.redirect('products');
		};
	});
};