
/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes')
  , product = require('./routes/product')
  , http = require('http')
  , path = require('path');

var app = express();

app.configure(function(){
  app.set('port', process.env.PORT || 3000);
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(path.join(__dirname, 'public')));
});

app.configure('development', function(){
  app.use(express.errorHandler());
});

app.get('/', routes.index);
app.get('/products', product.list);
app.get('/products/new', product.new);
app.post('/product', product.create);
app.get('/product/:id/edit', product.edit);
app.put('/product/:id/update', product.update);
app.get('/product/:id', product.show);
app.delete('/product/:id/delete', product.delete);

http.createServer(app).listen(app.get('port'), function(){
  console.log("Express server listening on port " + app.get('port'));
});
